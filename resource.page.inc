<?php

/**
 * @file
 * Contains resource.page.inc.
 *
 * Page callback for Resource entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Resource templates.
 *
 * Default template: resource.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_resource(array &$variables) {
  // Fetch Resource Entity Object.
  $resource = $variables['elements']['#resource'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
