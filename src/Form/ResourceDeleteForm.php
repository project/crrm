<?php

namespace Drupal\crrm\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Resource entities.
 *
 * @ingroup crrm
 */
class ResourceDeleteForm extends ContentEntityDeleteForm {


}
