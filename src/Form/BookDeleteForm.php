<?php

namespace Drupal\crrm\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Book entities.
 *
 * @ingroup crrm
 */
class BookDeleteForm extends ContentEntityDeleteForm {


}
