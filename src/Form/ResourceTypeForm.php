<?php

namespace Drupal\crrm\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ResourceTypeForm.
 */
class ResourceTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $resource_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $resource_type->label(),
      '#description' => $this->t("Label for the Resource type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $resource_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\crrm\Entity\ResourceType::load',
      ],
      '#disabled' => !$resource_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $resource_type = $this->entity;
    $status = $resource_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Resource type.', [
          '%label' => $resource_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Resource type.', [
          '%label' => $resource_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($resource_type->toUrl('collection'));
  }

}
