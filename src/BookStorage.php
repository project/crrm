<?php

namespace Drupal\crrm;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\crrm\Entity\BookInterface;

/**
 * Defines the storage handler class for Book entities.
 *
 * This extends the base storage class, adding required special handling for
 * Book entities.
 *
 * @ingroup crrm
 */
class BookStorage extends SqlContentEntityStorage implements BookStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(BookInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {book_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {book_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(BookInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {book_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('book_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
