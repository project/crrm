<?php

namespace Drupal\crrm;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\crrm\Entity\BookInterface;

/**
 * Defines the storage handler class for Book entities.
 *
 * This extends the base storage class, adding required special handling for
 * Book entities.
 *
 * @ingroup crrm
 */
interface BookStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Book revision IDs for a specific Book.
   *
   * @param \Drupal\crrm\Entity\BookInterface $entity
   *   The Book entity.
   *
   * @return int[]
   *   Book revision IDs (in ascending order).
   */
  public function revisionIds(BookInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Book author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Book revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\crrm\Entity\BookInterface $entity
   *   The Book entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(BookInterface $entity);

  /**
   * Unsets the language for all Book with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
