<?php

namespace Drupal\crrm;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for book.
 */
class BookTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
