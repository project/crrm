<?php

namespace Drupal\crrm;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\crrm\Entity\ResourceInterface;

/**
 * Defines the storage handler class for Resource entities.
 *
 * This extends the base storage class, adding required special handling for
 * Resource entities.
 *
 * @ingroup crrm
 */
class ResourceStorage extends SqlContentEntityStorage implements ResourceStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ResourceInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {resource_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {resource_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ResourceInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {resource_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('resource_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
