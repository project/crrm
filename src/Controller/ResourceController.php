<?php

namespace Drupal\crrm\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\crrm\Entity\ResourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ResourceController.
 *
 *  Returns responses for Resource routes.
 */
class ResourceController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Resource revision.
   *
   * @param int $resource_revision
   *   The Resource revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($resource_revision) {
    $resource = $this->entityTypeManager()->getStorage('resource')
      ->loadRevision($resource_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('resource');

    return $view_builder->view($resource);
  }

  /**
   * Page title callback for a Resource revision.
   *
   * @param int $resource_revision
   *   The Resource revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($resource_revision) {
    $resource = $this->entityTypeManager()->getStorage('resource')
      ->loadRevision($resource_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $resource->label(),
      '%date' => $this->dateFormatter->format($resource->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Resource.
   *
   * @param \Drupal\crrm\Entity\ResourceInterface $resource
   *   A Resource object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ResourceInterface $resource) {
    $account = $this->currentUser();
    $resource_storage = $this->entityTypeManager()->getStorage('resource');

    $langcode = $resource->language()->getId();
    $langname = $resource->language()->getName();
    $languages = $resource->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $resource->label()]) : $this->t('Revisions for %title', ['%title' => $resource->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all resource revisions") || $account->hasPermission('administer resource entities')));
    $delete_permission = (($account->hasPermission("delete all resource revisions") || $account->hasPermission('administer resource entities')));

    $rows = [];

    $vids = $resource_storage->revisionIds($resource);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\crrm\Entity\ResourceInterface $revision */
      $revision = $resource_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $resource->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.resource.revision', [
            'resource' => $resource->id(),
            'resource_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $resource->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.resource.translation_revert', [
                'resource' => $resource->id(),
                'resource_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.resource.revision_revert', [
                'resource' => $resource->id(),
                'resource_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.resource.revision_delete', [
                'resource' => $resource->id(),
                'resource_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['resource_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
