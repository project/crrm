<?php

namespace Drupal\crrm\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\crrm\Entity\BookInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BookController.
 *
 *  Returns responses for Book routes.
 */
class BookController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Book revision.
   *
   * @param int $book_revision
   *   The Book revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($book_revision) {
    $book = $this->entityTypeManager()->getStorage('book')
      ->loadRevision($book_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('book');

    return $view_builder->view($book);
  }

  /**
   * Page title callback for a Book revision.
   *
   * @param int $book_revision
   *   The Book revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($book_revision) {
    $book = $this->entityTypeManager()->getStorage('book')
      ->loadRevision($book_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $book->label(),
      '%date' => $this->dateFormatter->format($book->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Book.
   *
   * @param \Drupal\crrm\Entity\BookInterface $book
   *   A Book object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(BookInterface $book) {
    $account = $this->currentUser();
    $book_storage = $this->entityTypeManager()->getStorage('book');

    $langcode = $book->language()->getId();
    $langname = $book->language()->getName();
    $languages = $book->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $book->label()]) : $this->t('Revisions for %title', ['%title' => $book->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all book revisions") || $account->hasPermission('administer book entities')));
    $delete_permission = (($account->hasPermission("delete all book revisions") || $account->hasPermission('administer book entities')));

    $rows = [];

    $vids = $book_storage->revisionIds($book);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\crrm\Entity\BookInterface $revision */
      $revision = $book_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $book->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.book.revision', [
            'book' => $book->id(),
            'book_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $book->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.book.translation_revert', [
                'book' => $book->id(),
                'book_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.book.revision_revert', [
                'book' => $book->id(),
                'book_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.book.revision_delete', [
                'book' => $book->id(),
                'book_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['book_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
