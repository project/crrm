<?php

namespace Drupal\crrm;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\crrm\Entity\ResourceInterface;

/**
 * Defines the storage handler class for Resource entities.
 *
 * This extends the base storage class, adding required special handling for
 * Resource entities.
 *
 * @ingroup crrm
 */
interface ResourceStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Resource revision IDs for a specific Resource.
   *
   * @param \Drupal\crrm\Entity\ResourceInterface $entity
   *   The Resource entity.
   *
   * @return int[]
   *   Resource revision IDs (in ascending order).
   */
  public function revisionIds(ResourceInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Resource author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Resource revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\crrm\Entity\ResourceInterface $entity
   *   The Resource entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ResourceInterface $entity);

  /**
   * Unsets the language for all Resource with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
