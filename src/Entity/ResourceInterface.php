<?php

namespace Drupal\crrm\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Resource entities.
 *
 * @ingroup crrm
 */
interface ResourceInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Resource name.
   *
   * @return string
   *   Name of the Resource.
   */
  public function getName();

  /**
   * Sets the Resource name.
   *
   * @param string $name
   *   The Resource name.
   *
   * @return \Drupal\crrm\Entity\ResourceInterface
   *   The called Resource entity.
   */
  public function setName($name);

  /**
   * Gets the Resource creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Resource.
   */
  public function getCreatedTime();

  /**
   * Sets the Resource creation timestamp.
   *
   * @param int $timestamp
   *   The Resource creation timestamp.
   *
   * @return \Drupal\crrm\Entity\ResourceInterface
   *   The called Resource entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Resource revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Resource revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\crrm\Entity\ResourceInterface
   *   The called Resource entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Resource revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Resource revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\crrm\Entity\ResourceInterface
   *   The called Resource entity.
   */
  public function setRevisionUserId($uid);

}
