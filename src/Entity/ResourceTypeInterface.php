<?php

namespace Drupal\crrm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Resource type entities.
 */
interface ResourceTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
