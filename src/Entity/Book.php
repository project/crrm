<?php

namespace Drupal\crrm\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Book entity.
 *
 * @ingroup crrm
 *
 * @ContentEntityType(
 *   id = "book",
 *   label = @Translation("Book"),
 *   handlers = {
 *     "storage" = "Drupal\crrm\BookStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\crrm\BookListBuilder",
 *     "views_data" = "Drupal\crrm\Entity\BookViewsData",
 *     "translation" = "Drupal\crrm\BookTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\crrm\Form\BookForm",
 *       "add" = "Drupal\crrm\Form\BookForm",
 *       "edit" = "Drupal\crrm\Form\BookForm",
 *       "delete" = "Drupal\crrm\Form\BookDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\crrm\BookHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\crrm\BookAccessControlHandler",
 *   },
 *   base_table = "book",
 *   data_table = "book_field_data",
 *   revision_table = "book_revision",
 *   revision_data_table = "book_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer book entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
*   revision_metadata_keys = {
*     "revision_user" = "revision_uid",
*     "revision_created" = "revision_timestamp",
*     "revision_log_message" = "revision_log"
*   },
 *   links = {
 *     "canonical" = "/admin/crrm/book/{book}",
 *     "add-form" = "/admin/crrm/book/add",
 *     "edit-form" = "/admin/crrm/book/{book}/edit",
 *     "delete-form" = "/admin/crrm/book/{book}/delete",
 *     "version-history" = "/admin/crrm/book/{book}/revisions",
 *     "revision" = "/admin/crrm/book/{book}/revisions/{book_revision}/view",
 *     "revision_revert" = "/admin/crrm/book/{book}/revisions/{book_revision}/revert",
 *     "revision_delete" = "/admin/crrm/book/{book}/revisions/{book_revision}/delete",
 *     "translation_revert" = "/admin/crrm/book/{book}/revisions/{book_revision}/revert/{langcode}",
 *     "collection" = "/admin/crrm/book",
 *   },
 *   field_ui_base_route = "book.settings"
 * )
 */
class Book extends EditorialContentEntityBase implements BookInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
      'resource_id' => \Drupal::request()->query->get('resource_id'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the book owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  //   /**$parameters = \Drupal::routeMatch()->getParameters();
  //  * {@inheritdoc}
  //  */
  // public function getBookDate() {
  //   return $this->get('book_date')->value;
  // }

  // /**
  //  * {@inheritdoc}
  //  */
  // public function setBookDate($timestamp) {
  //   $this->set('book_date', $timestamp);
  //   return $this;
  // }
  /**
   * {@inheritdoc}
   */
  public function getResource() {
    return $this->get('resource_id')->entity;
  }
  /**
   * {@inheritdoc}
   */
  public function getResourceId() {
    return $this->get('resource_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setResourceId($nid) {
    $this->set('resource_id', $nid);
    return $this;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Book entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Book entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);
    
    // $rangedefaults['value'] = $rangedefaults['end_value'] = (new DrupalDateTime())->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    // $fields['book_date'] = BaseFieldDefinition::create('smartdate')
    //   ->setLabel(t('Date and time'))
    //   ->setDescription(t('When the activity takes place.'))
    //   ->setRequired(false)
    //   ->setDisplayOptions('view', [
    //     'type' => 'smartdate_recurring',
    //     'label' => 'hidden',
    //     'weight' => -3,
    //   ])
    //   ->setDisplayOptions('form', [
    //     'type' => 'smart_date_recur_inline',
    //     'weight' => -3,
    //     'settings' => [
    //       'modal' => FALSE,
    //       'default_tz' => 'user',
    //     ]
    //   ])
    //   ->setDefaultValue([
    //     'default_date_type' => 'next_hour',
    //     'default_date' => '',
    //     'default_duration_increments' => "0 | No End Time\r\n30\r\n60|1 hour\r\n90\r\n120|2 hours\r\ncustom",
    //     'default_duration' => '30',
    //   ])
    //   ->setSetting('allow_recurring', TRUE)
    //   ->setSetting('month_limit', 12)
    //   ->setSetting('show_extra', TRUE)
    //   ->setSetting('hide_date', FALSE)
    //   ->setSetting('allday', TRUE)
    //   ->setSetting('remove_seconds', FALSE)
    //   ->setDisplayConfigurable('form', TRUE)
    //   ->setDisplayConfigurable('view', TRUE)
    //   ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $resourcedefaults = \Drupal::request()->query->get('resource_id');

    $fields['resource_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Resource'))
      ->setDescription(t('The resource ID, reservation was created for.'))
      ->setSetting('target_type', 'resource')
      ->setSetting('handler', 'default')
      ->setTargetEntityTypeId('resource')
      ->setDisplayOptions('view', array(
        'label'  => 'hidden',
        'type'   => 'resource',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDefaultValue($resourcedefaults);

    $fields['status']->setDescription(t('A boolean indicating whether the Book is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
