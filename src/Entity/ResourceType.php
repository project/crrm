<?php

namespace Drupal\crrm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Resource type entity.
 *
 * @ConfigEntityType(
 *   id = "resource_type",
 *   label = @Translation("Resource type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\crrm\ResourceTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\crrm\Form\ResourceTypeForm",
 *       "edit" = "Drupal\crrm\Form\ResourceTypeForm",
 *       "delete" = "Drupal\crrm\Form\ResourceTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\crrm\ResourceTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   config_prefix = "resource_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "resource",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/crrm/resource_type/{resource_type}",
 *     "add-form" = "/admin/crrm/resource_type/add",
 *     "edit-form" = "/admin/crrm/resource_type/{resource_type}/edit",
 *     "delete-form" = "/admin/crrm/resource_type/{resource_type}/delete",
 *     "collection" = "/admin/crrm/resource_type"
 *   }
 * )
 */
class ResourceType extends ConfigEntityBundleBase implements ResourceTypeInterface {

  /**
   * The Resource type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Resource type label.
   *
   * @var string
   */
  protected $label;

}
