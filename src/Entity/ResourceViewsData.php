<?php

namespace Drupal\crrm\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Resource entities.
 */
class ResourceViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
