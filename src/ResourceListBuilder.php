<?php

namespace Drupal\crrm;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Resource entities.
 *
 * @ingroup crrm
 */
class ResourceListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Resource ID');
    $header['type'] = $this->t('Type');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\crrm\Entity\Resource $entity */
    $row['id'] = $entity->id();
    $row['type'] = $entity->type->getValue()[0]['target_id'];
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.resource.canonical',
      ['resource' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
